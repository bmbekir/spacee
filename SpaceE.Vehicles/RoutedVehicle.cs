﻿using SpaceE.Geo;
using System;
using System.Collections.Generic;
using System.Text;

namespace SpaceE.Vehicles
{
    class RoutedVehicle
    {
        public IVehicle Vehicle { get; set; }
        public Route Route { get; set; }

        internal bool Completed { get; set; }
        public RoutedVehicle(IVehicle vehicle, Route route)
        {
            this.Vehicle = vehicle;
            this.Route = route;
        }
    }
}
