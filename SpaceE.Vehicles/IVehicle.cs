﻿using SpaceE.Geo;
using System;
using System.Collections.Generic;
using System.Text;

namespace SpaceE.Vehicles
{
    public interface IVehicle:ISpaceThing
    {
        Coordinate Coordinate { get; set; }
    }
}
