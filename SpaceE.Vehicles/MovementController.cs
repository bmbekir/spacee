﻿using SpaceE.Geo;
using System;
using System.Collections.Generic;
using System.Text;

namespace SpaceE.Vehicles
{
    public class MovementController
    {
        private static Dictionary<Direction, Direction> rightRotationNext;
        private static Dictionary<Direction, Direction> leftRotationNext;

        private static Dictionary<Direction, int> moveXValues;
        private static Dictionary<Direction, int> moveYValues;

        static MovementController()
        {
            rightRotationNext = new Dictionary<Direction, Direction>();
            leftRotationNext = new Dictionary<Direction, Direction>();

            var values = Enum.GetValues(typeof(Direction));
            Direction value;
            for (int i = 0; i < values.Length; i++)
            {
                value = (Direction)values.GetValue(i);
                rightRotationNext[value] = (Direction)values.GetValue((i + 1) % values.Length);
                leftRotationNext[value] = (Direction)values.GetValue((i - 1 + values.Length) % values.Length);
            }

            moveXValues = new Dictionary<Direction, int>
            {
                {Direction.North, 0},
                {Direction.East, 1},
                {Direction.South, 0},
                {Direction.West, -1},
            };

            moveYValues = new Dictionary<Direction, int>
            {
                {Direction.North, 1},
                {Direction.East, 0},
                {Direction.South, -1},
                {Direction.West, 0},
            };
        }


        public bool Move(IVehicle vehicle, MovementDirection direction, AreaMatrix matrix)
        {
            switch (direction)
            {
                case MovementDirection.RotateLeft:
                    return RotateLeft(vehicle);
                case MovementDirection.RotateRight:
                    return RotateRight(vehicle);
                case MovementDirection.Move:
                    return Move(vehicle, matrix);
                default:
                    return false;
            }
        }

        public bool Move(IVehicle vehicle, AreaMatrix matrix)
        {
            var coordinate = new Coordinate(vehicle.Coordinate.X + moveXValues[vehicle.Coordinate.Direction]
                                  , vehicle.Coordinate.Y + moveYValues[vehicle.Coordinate.Direction]
                                  , vehicle.Coordinate.Direction);
            if (matrix.IsUsablePosition(coordinate.X, coordinate.Y))
            {
                vehicle.Coordinate = coordinate;
                return true;
            }
            return false;
        }

        public bool RotateRight(IVehicle vehicle)
        {
            vehicle.Coordinate = new Coordinate(vehicle.Coordinate.X, vehicle.Coordinate.Y, rightRotationNext[vehicle.Coordinate.Direction]);
            return true;
        }


        public bool RotateLeft(IVehicle vehicle)
        {
            vehicle.Coordinate = new Coordinate(vehicle.Coordinate.X, vehicle.Coordinate.Y, leftRotationNext[vehicle.Coordinate.Direction]);
            return true;
        }
    }
}
