﻿using SpaceE.Geo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpaceE.Vehicles
{
    public class SyncMovementSimulator
    {
        private List<RoutedVehicle> vehicles;
        private AreaMatrix matrix;
        private MovementController controller;
        private int activeIndex;

        public SyncMovementSimulator(AreaMatrix matrix)
        {
            this.matrix = matrix;
            this.vehicles = new List<RoutedVehicle>();
            this.controller = new MovementController();
            this.activeIndex = 0;
        }

        /// <summary>
        /// Add vehicle wtih route
        /// </summary>
        /// <param name="vehicle"></param>
        /// <param name="route"></param>
        public void Add(IVehicle vehicle, Route route)
        {
            this.vehicles.Add(new RoutedVehicle(vehicle, route));
        }

        /// <summary>
        /// run simulation a step
        /// </summary>
        /// <returns></returns>
        public bool Step()
        {
            var result = false;
            foreach (var item in vehicles.Where(b => !b.Completed))
            {
                controller.Move(item.Vehicle, item.Route.GetCurrent(), matrix);
                item.Completed = !item.Route.HasNext();
                if (!item.Completed)
                {
                    result = true;
                    item.Route.GoNext();
                }
            }
            activeIndex++;
            return result;
        }


        /// <summary>
        /// run simulation all steps
        /// </summary>
        public void All()
        {
            bool next;
            do
            {
                next = Step();
            } while (next);
        }

        public void Reset()
        {
            this.activeIndex = 0;
            foreach (var item in vehicles)
            {
                item.Route.Reset();
                item.Completed = false;
            }
        }
    }
}
