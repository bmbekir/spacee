﻿using System;
using SpaceE.Geo;

namespace SpaceE.Vehicles
{
    public class Rover : IVehicle
    {
        private Coordinate coordinate;
        public Rover(Coordinate coordinate)
        {
            this.coordinate = coordinate;
        }

        public Coordinate Coordinate { get => coordinate; set => coordinate = value; }

        public int PositionX => coordinate.X;

        public int PositionY => this.coordinate.Y;

        public Coordinate GetCoordinate()
        {
            return this.coordinate.Clone();
        }

    }
}
