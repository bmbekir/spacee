﻿using System;

namespace SpaceE.Geo
{
    public class AreaMatrix
    {
 
        private readonly int width;
        private readonly int length;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="width"> Width of Matrix</param>
        /// <param name="length">Length of Matrix</param>
        public AreaMatrix(int width, int length)
        {
            this.width = width;
            this.length = length;
        }

        /// <summary>
        /// Width of Matrix
        /// </summary>
        public int Width { get => width; }
        /// <summary>
        /// Length of Matrix
        /// </summary>
        public int Length { get => length; }

        /// <summary>
        /// The position is in area
        /// </summary>
        /// <param name="posX">Postion X value</param>
        /// <param name="posY">Position Y value</param>
        /// <returns></returns>
        public bool IsUsablePosition(int posX, int posY)
        {
            return true;
            return posX < width && posX >= 0
              && posY < length && posY >= 0;
        }

        /// <summary>
        /// The thing is in area
        /// </summary>
        /// <param name="thing"> a thing</param>
        /// <returns></returns>
        public bool IsUsablePosition(ISpaceThing thing)
        {
            return IsUsablePosition(thing.PositionX, thing.PositionY);
        }

    }
}
