﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpaceE.Geo
{
    public class Route
    {
        private readonly List<MovementDirection> directions;
        private int activeRoute;
        public Route()
        {
            directions = new List<MovementDirection>();
            activeRoute = 0;
        }

        public Route(MovementDirection[] directions) : this()
        {
            this.directions.AddRange(directions);
        }


        /// <summary>
        /// Route has next value
        /// </summary>
        /// <returns></returns>
        public bool HasNext()
        {
            return activeRoute < directions.Count - 1;
        }

        /// <summary>
        /// Route go to next value
        /// </summary>
        /// <returns>next value</returns>
        public MovementDirection GoNext()
        {
            if (HasNext())
            {
                activeRoute++;
            }
            return GetCurrent();
        }
        /// <summary>
        /// get current route value
        /// </summary>
        /// <returns></returns>
        public MovementDirection GetCurrent()
        {
            return directions[activeRoute];
        }

        public int Count()
        {
            return directions.Count;
        }

        public void Reset()
        {
            this.activeRoute = 0;
        }
    }
}
