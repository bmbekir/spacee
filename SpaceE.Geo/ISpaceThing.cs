﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpaceE.Geo
{
    public interface ISpaceThing
    {
        int PositionX { get; }
        int PositionY { get; }
    }
}
