﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpaceE.Geo
{
    public class Coordinate
    {
        public Coordinate() { }
        public Coordinate(int x, int y, Direction direction)
        {
            this.X = x;
            this.Y = y;
            this.Direction = direction;
        }
        /// <summary>
        /// Position X
        /// </summary>
        public int X { get; set; }
        /// <summary>
        /// Position Y
        /// </summary>
        public int Y { get; set; }
        /// <summary>
        /// Direction for coordinate
        /// </summary>
        public Direction Direction { get; set; }

        public Coordinate Clone()
        {
            return new Coordinate(this.X, this.Y, this.Direction);
        }
    }
}
