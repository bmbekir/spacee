﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpaceE.Geo
{
    /// <summary>
    /// Moevement Command Types
    /// </summary>
    public enum MovementDirection
    {
        /// <summary>
        /// Rotate right (clockwise) 90 degree
        /// </summary>
        RotateLeft = 1,
        /// <summary>
        /// Rotate left (counterclockwise) 90 degree
        /// </summary>
        RotateRight = 2,
        /// <summary>
        /// Go to next area
        /// </summary>
        Move= 3
    }
}
