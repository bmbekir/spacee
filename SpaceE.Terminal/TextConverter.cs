﻿using SpaceE.Geo;
using SpaceE.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpaceE.Terminal
{
    public class TextConverter
    {
        static Dictionary<string, Direction> textDirections;
        static Dictionary<char, MovementDirection> textMovementDirections;
        static Dictionary<Direction, String> directionTexts;

        static TextConverter()
        {
            textDirections = new Dictionary<string, Direction>()
            {
                {"N",Direction.North },
                {"E",Direction.East },
                {"S",Direction.South },
                {"W",Direction.West }
            };

            directionTexts = new Dictionary<Direction, string>
            {
                {Direction.North,"N" },
                {Direction.East,"E" },
                {Direction.South,"S" },
                {Direction.West,"W" }
            };

            textMovementDirections = new Dictionary<char, MovementDirection>
            {
                {'L',MovementDirection.RotateLeft },
                {'R',MovementDirection.RotateRight },
                {'M',MovementDirection.Move },
            };
        }

        /// <summary>
        /// Convert text to areamatrix
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public AreaMatrix TextToMatrix(string text)
        {
            var sizes = text.Split(" ").Select(b => Convert.ToInt32(b)).ToArray();
            if (sizes.Length != 2)
                throw new Exception("Wrong Input!");
            return new AreaMatrix(sizes[0], sizes[1]);
        }
        /// <summary>
        /// Convert text to route
        /// </summary>
        /// <param name="routeText">Route text</param>
        /// <returns></returns>
        public Route TextToRoute(string routeText)
        {
            routeText = routeText.Trim().ToUpper();
            MovementDirection[] directions = new MovementDirection[routeText.Length];
            try
            {
                for (int i = 0; i < routeText.Length; i++)
                {
                    directions[i] = textMovementDirections[routeText[i]];
                }
            }
            catch (Exception)
            {
                throw new Exception("Wrong Text");
            }

            return new Route(directions);
        }

        /// <summary>
        /// Convert coordinate text to rover
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public Rover TextToRover(string text)
        {
            text = text.Trim().ToUpper();
            var values = text.Split(" ");
            if (values.Length != 3)
                throw new Exception("Wrong Text");

            try
            {
                int x = Convert.ToInt32(values[0]);
                int y = Convert.ToInt32(values[1]);
                Direction d = textDirections[values[2].ToUpper()];
                return new Rover(new Coordinate(x, y, d));
            }
            catch (Exception)
            {
                throw new Exception("Wrong Text");
            }
        }

        /// <summary>
        /// Coordinate text to string
        /// </summary>
        /// <param name="coordinate"></param>
        /// <returns></returns>
        public string CoordinateToString(Coordinate coordinate)
        {
            return $"{coordinate.X} {coordinate.Y} {directionTexts[coordinate.Direction]}";
        }
    }
}
