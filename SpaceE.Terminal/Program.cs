﻿using SpaceE.Geo;
using SpaceE.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SpaceE.Terminal
{
    class Program
    {

        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to SpaceE (Space of Ebubekir) Rover Control System");
            var textConverter = new TextConverter();

            #region read matrix area 
            var matrix = ConsoleReader.ReadAction("Write Your Mars Area Size:", (text) => textConverter.TextToMatrix(text));
            #endregion

            #region read rovers and postions
            int roverCount = 2;
            Rover[] rovers = new Rover[roverCount];
            Rover rover;
            Route route;
            SyncMovementSimulator sim = new SyncMovementSimulator(matrix);

            for (int i = 0; i < roverCount; i++)
            {
                rover = ConsoleReader.ReadAction($"Write Rover {i + 1} Coordinates:", (text) => textConverter.TextToRover(text));
                route = ConsoleReader.ReadAction($"Write Rover {i + 1} Routes:", (text) => textConverter.TextToRoute(text));
                sim.Add(rover, route);
                rovers[i] = rover;
            }
            #endregion

            sim.All();

            #region write to results
            foreach (var item in rovers)
            {
                Console.WriteLine(textConverter.CoordinateToString(item.GetCoordinate()));
            }

            #endregion
            Console.ReadKey();
            Environment.Exit(0);
        }
    }
}
