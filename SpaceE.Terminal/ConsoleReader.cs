﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpaceE.Terminal
{
    static class ConsoleReader
    {
        public static T ReadAction<T>(string description, Func<string, T> func, bool loop=true)
        {
            string text;
            while (true)
            {
                Console.Write(description);
                text = Console.ReadLine();
                try
                {
                    return func.Invoke(text);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    if (!loop)
                        return default(T);
                }
            }
        }
    }
}
