using SpaceE.Geo;
using SpaceE.Vehicles;
using System;
using Xunit;

namespace SpaceE.UnitTests.Vehicle
{
    public class MovementControllerTest
    {
        private readonly MovementController controller;
        public MovementControllerTest()
        {
            this.controller = new MovementController();
        }

        [Fact]
        public void RotateRightTest()
        {
            var rover = new Rover(new Coordinate(0, 0, Direction.East));
            this.controller.RotateRight(rover);
            Assert.True(rover.Coordinate.Direction == Direction.South);
            this.controller.RotateRight(rover);
            Assert.True(rover.Coordinate.Direction == Direction.West);
            this.controller.RotateRight(rover);
            Assert.True(rover.Coordinate.Direction == Direction.North);
            this.controller.RotateRight(rover);
            Assert.True(rover.Coordinate.Direction == Direction.East);
        }

        [Fact]
        public void RotateLefTtest()
        {
            var rover = new Rover(new Coordinate(0, 0, Direction.West));
            this.controller.RotateLeft(rover);
            Assert.True(rover.Coordinate.Direction == Direction.South);
            this.controller.RotateLeft(rover);
            Assert.True(rover.Coordinate.Direction == Direction.East);
            this.controller.RotateLeft(rover);
            Assert.True(rover.Coordinate.Direction == Direction.North);
            this.controller.RotateLeft(rover);
            Assert.True(rover.Coordinate.Direction == Direction.West);
        }

        [Fact]
        public void MoveTest()
        {
            var rover = new Rover(new Coordinate(0, 0, Direction.East));
            var matrix = new AreaMatrix(3, 3);
            this.controller.Move(rover, matrix);
            Assert.True(rover.Coordinate.Direction == Direction.East);
            Assert.True(rover.Coordinate.X == 1);
            Assert.True(rover.Coordinate.Y == 0);

            this.controller.Move(rover, matrix);
            Assert.True(rover.Coordinate.Direction == Direction.East);
            Assert.True(rover.Coordinate.X == 2);
            Assert.True(rover.Coordinate.Y == 0);

            //this.controller.Move(rover, matrix);
            //Assert.True(rover.Coordinate.Direction == Direction.East);
            //Assert.True(rover.Coordinate.X == 2);
            //Assert.True(rover.Coordinate.Y == 0);
        }

        //[Fact]
        //public void MoveTestNegative()
        //{
        //    var rover = new Rover(new Coordinate(0, 0, Direction.West));
        //    var matrix = new AreaMatrix(3, 3);
        //    this.controller.Move(rover, matrix);
        //    Assert.True(rover.Coordinate.Direction == Direction.West);
        //    Assert.True(rover.Coordinate.X == 0);
        //    Assert.True(rover.Coordinate.Y == 0);
        //}

        [Fact]
        public void ClockwiseSpinTest()
        {
            var rover = new Rover(new Coordinate(0, 0, Direction.North));
            var matrix = new AreaMatrix(3, 3);
            this.controller.Move(rover,MovementDirection.Move, matrix);
            Assert.True(rover.Coordinate.Direction == Direction.North);
            Assert.True(rover.Coordinate.X == 0);
            Assert.True(rover.Coordinate.Y == 1);

            this.controller.Move(rover,MovementDirection.RotateRight, matrix);
            Assert.True(rover.Coordinate.Direction == Direction.East);
            Assert.True(rover.Coordinate.X == 0);
            Assert.True(rover.Coordinate.Y == 1);

            this.controller.Move(rover, MovementDirection.Move, matrix);
            Assert.True(rover.Coordinate.Direction == Direction.East);
            Assert.True(rover.Coordinate.X == 1);
            Assert.True(rover.Coordinate.Y == 1);

            this.controller.Move(rover, MovementDirection.RotateRight, matrix);
            Assert.True(rover.Coordinate.Direction == Direction.South);
            Assert.True(rover.Coordinate.X == 1);
            Assert.True(rover.Coordinate.Y == 1);

            this.controller.Move(rover, MovementDirection.Move, matrix);
            Assert.True(rover.Coordinate.Direction == Direction.South);
            Assert.True(rover.Coordinate.X == 1);
            Assert.True(rover.Coordinate.Y == 0);


            this.controller.Move(rover, MovementDirection.RotateRight, matrix);
            Assert.True(rover.Coordinate.Direction == Direction.West);
            Assert.True(rover.Coordinate.X == 1);
            Assert.True(rover.Coordinate.Y == 0);

            this.controller.Move(rover, MovementDirection.Move, matrix);
            Assert.True(rover.Coordinate.Direction == Direction.West);
            Assert.True(rover.Coordinate.X == 0);
            Assert.True(rover.Coordinate.Y == 0);

            this.controller.Move(rover, MovementDirection.RotateRight, matrix);
            Assert.True(rover.Coordinate.Direction == Direction.North);
            Assert.True(rover.Coordinate.X == 0);
            Assert.True(rover.Coordinate.Y == 0);

        }


        [Fact]
        public void CounterclockwiseSpinTest()
        {
            var rover = new Rover(new Coordinate(1, 1, Direction.North));
            var matrix = new AreaMatrix(3, 3);
            this.controller.Move(rover, MovementDirection.Move, matrix);
            Assert.True(rover.Coordinate.Direction == Direction.North);
            Assert.True(rover.Coordinate.X == 1);
            Assert.True(rover.Coordinate.Y == 2);

            this.controller.Move(rover, MovementDirection.RotateLeft, matrix);
            Assert.True(rover.Coordinate.Direction == Direction.West);
            Assert.True(rover.Coordinate.X == 1);
            Assert.True(rover.Coordinate.Y == 2);

            this.controller.Move(rover, MovementDirection.Move, matrix);
            Assert.True(rover.Coordinate.Direction == Direction.West);
            Assert.True(rover.Coordinate.X == 0);
            Assert.True(rover.Coordinate.Y == 2);

            this.controller.Move(rover, MovementDirection.RotateLeft, matrix);
            Assert.True(rover.Coordinate.Direction == Direction.South);
            Assert.True(rover.Coordinate.X == 0);
            Assert.True(rover.Coordinate.Y == 2);

            this.controller.Move(rover, MovementDirection.Move, matrix);
            Assert.True(rover.Coordinate.Direction == Direction.South);
            Assert.True(rover.Coordinate.X == 0);
            Assert.True(rover.Coordinate.Y == 1);


            this.controller.Move(rover, MovementDirection.RotateLeft, matrix);
            Assert.True(rover.Coordinate.Direction == Direction.East);
            Assert.True(rover.Coordinate.X == 0);
            Assert.True(rover.Coordinate.Y == 1);

            this.controller.Move(rover, MovementDirection.Move, matrix);
            Assert.True(rover.Coordinate.Direction == Direction.East);
            Assert.True(rover.Coordinate.X == 1);
            Assert.True(rover.Coordinate.Y == 1);

            this.controller.Move(rover, MovementDirection.RotateLeft, matrix);
            Assert.True(rover.Coordinate.Direction == Direction.North);
            Assert.True(rover.Coordinate.X == 1);
            Assert.True(rover.Coordinate.Y == 1);

        }
    }
}
