﻿using SpaceE.Geo;
using SpaceE.Vehicles;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace SpaceE.UnitTests.Vehicle
{
    public class SyncMovementSimulatorTest
    {
        [Fact]
        public void AllTest()
        {
            var matrix = new AreaMatrix(5, 5);
            var rovers = new Rover[2];
            var sim = new SyncMovementSimulator(matrix);
            rovers[0] = new Rover(new Coordinate(1, 2, Direction.North));
            sim.Add(rovers[0], new Route(new MovementDirection[]
            {
                MovementDirection.RotateLeft,
                MovementDirection.Move,
                MovementDirection.RotateLeft,
                MovementDirection.Move,
                MovementDirection.RotateLeft,
                MovementDirection.Move,
                MovementDirection.RotateLeft,
                MovementDirection.Move,
                MovementDirection.Move,
            }));

            rovers[1] = new Rover(new Coordinate(3, 3, Direction.East));

            sim.Add(rovers[1], new Route(new MovementDirection[]
            {
                MovementDirection.Move,
                MovementDirection.Move,
                MovementDirection.RotateRight,
                MovementDirection.Move,
                MovementDirection.Move,
                MovementDirection.RotateRight,
                MovementDirection.Move,
                MovementDirection.RotateRight,
                MovementDirection.RotateRight,
                MovementDirection.Move
            }));
            sim.All();
            Assert.True(rovers[0].PositionX == 1);
            Assert.True(rovers[0].PositionY == 3);
            Assert.True(rovers[0].Coordinate.Direction == Direction.North);


            Assert.True(rovers[1].PositionX == 5);
            Assert.True(rovers[1].PositionY == 1);
            Assert.True(rovers[1].Coordinate.Direction == Direction.East);
        }
    }
}
