﻿using SpaceE.Geo;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace SpaceE.UnitTests.Geo
{
    public class RouteTest
    {

        public RouteTest()
        {
        }

        [Fact]
        public void HasNextTest()
        {
            var route = new Route(new MovementDirection[] {
                MovementDirection.Move,
                MovementDirection.RotateLeft,
                MovementDirection.RotateRight,
            });
            Assert.True(route.HasNext(), "Next 1 should be true");
            route.GoNext();
            Assert.True(route.HasNext(), "Next 2 should be true");
            route.GoNext();
            Assert.False(route.HasNext(), "Next 3 should be false");
        }


        [Fact]
        public void GoNextTest()
        {
            var route = new Route(new MovementDirection[] {
                MovementDirection.Move,
                MovementDirection.RotateLeft,
                MovementDirection.RotateRight,
            });
            Assert.True(route.GoNext() == MovementDirection.RotateLeft, "Go 1 should be left");
            Assert.True(route.GoNext() == MovementDirection.RotateRight, "Go 2 should be right");
            Assert.True(route.GoNext() == MovementDirection.RotateRight, "Go 3 should be right");
        }


        [Fact]
        public void CurrentTest()
        {
            var route = new Route(new MovementDirection[] {
                MovementDirection.Move,
                MovementDirection.RotateLeft,
                MovementDirection.RotateRight,
            });
            Assert.True(route.GetCurrent() == MovementDirection.Move, "Current 1 should be move");
            route.GoNext();
            Assert.True(route.GetCurrent() == MovementDirection.RotateLeft, "Current 2 should be right");
            route.GoNext();
            Assert.True(route.GetCurrent() == MovementDirection.RotateRight, "Current 3 should be right");
            route.GoNext();
            Assert.True(route.GetCurrent() == MovementDirection.RotateRight, "Current 4 should be right");
        }
    }
}
