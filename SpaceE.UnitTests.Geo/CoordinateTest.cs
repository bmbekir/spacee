﻿using SpaceE.Geo;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace SpaceE.UnitTests.Geo
{
    public class CoordinateTest
    {
        private readonly Coordinate coordinate;
        public CoordinateTest()
        {
            this.coordinate = new Coordinate(1, 3, Direction.East);
        }

        [Fact]
        public void CloneTest()
        {
            var clone = this.coordinate.Clone();
            Assert.True(clone.X == this.coordinate.X);
            Assert.True(clone.Y == this.coordinate.Y);
            Assert.True(clone.Direction == this.coordinate.Direction);
            Assert.False(clone == this.coordinate);

        }
    }
}
