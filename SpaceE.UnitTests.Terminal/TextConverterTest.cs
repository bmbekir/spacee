using SpaceE.Terminal;
using System;
using System.Collections.Generic;
using Xunit;
namespace SpaceE.UnitTests.Terminal
{
    public class TextConverterTest
    {
        private readonly TextConverter converter;

        public TextConverterTest()
        {
            this.converter = new TextConverter();
        }
        [Fact]
        public void TextToRouteTest()
        {
            var route = this.converter.TextToRoute("LMRLMRRMMLL");
            Assert.True(route.GetCurrent() == Geo.MovementDirection.RotateLeft);
            Assert.True(route.GoNext() == Geo.MovementDirection.Move);
            Assert.True(route.GoNext() == Geo.MovementDirection.RotateRight);
            Assert.True(route.GoNext() == Geo.MovementDirection.RotateLeft);
            Assert.True(route.GoNext() == Geo.MovementDirection.Move);
            Assert.True(route.GoNext() == Geo.MovementDirection.RotateRight);
            Assert.True(route.GoNext() == Geo.MovementDirection.RotateRight);
            Assert.True(route.GoNext() == Geo.MovementDirection.Move);
            Assert.True(route.GoNext() == Geo.MovementDirection.Move);
            Assert.True(route.GoNext() == Geo.MovementDirection.RotateLeft);
            Assert.True(route.GoNext() == Geo.MovementDirection.RotateLeft);
        }

        [Fact]
        public void TextToRouteExceptionTest()
        {
            Assert.Throws<Exception>(() => this.converter.TextToRoute("ALMRLMRRMMLL"));
        }

        [Fact]
        public void TextToRoverTest()
        {
            var rover = this.converter.TextToRover("1 2 N");
            Assert.True(rover.Coordinate.X == 1);
            Assert.True(rover.Coordinate.Y == 2);
            Assert.True(rover.Coordinate.Direction == Geo.Direction.North);
        }

        [Fact]
        public void TextToRoverExceptionTest()
        {
            Assert.Throws<Exception>(() => this.converter.TextToRover("12 E"));
            Assert.Throws<Exception>(() => this.converter.TextToRover("1 2 A"));
            Assert.Throws<Exception>(() => this.converter.TextToRover("E1 2"));
        }


        [Fact]
        public void CoordinateToStringTest()
        {
            Assert.True(converter.CoordinateToString(new Geo.Coordinate(1, 0, Geo.Direction.East)) == "1 0 E");
            Assert.True(converter.CoordinateToString(new Geo.Coordinate(3, 5, Geo.Direction.West)) == "3 5 W");
            Assert.True(converter.CoordinateToString(new Geo.Coordinate(2, 2, Geo.Direction.South)) == "2 2 S");
            Assert.True(converter.CoordinateToString(new Geo.Coordinate(1, 2, Geo.Direction.North)) == "1 2 N");
        }

        [Fact]
        public void CoordinateToStringExceptionTest()
        {
            Assert.Throws<KeyNotFoundException>(() => converter.CoordinateToString(new Geo.Coordinate(1, 0, (Geo.Direction)0)));
        }

    }
}
